package utils

func IterativeDigitsCount(number uint64) uint64 {  
	count := uint64(0)
	for number != 0 {
		number /= 10  
		count += 1
	}
	return count
}