package utils

import (
	"net/http"
	"html/template"
	templ "github.com/bharat-rajani/go-fstack/web/template"
)

type Msg struct{
	Message string
}

func ErrorRenderer(w http.ResponseWriter, message string){
	tmpl := template.Must(templ.ErrorHtml())

	tmpl.Execute(w, Msg{Message: message})
}