package middlewares

import (
	"github.com/bharat-rajani/go-fstack/internal/utils"
	"errors"
	"net/http"
	"log"

	"github.com/bharat-rajani/go-fstack/internal/auth"
	"github.com/bharat-rajani/go-fstack/internal/responses"
)

/* SetMiddlewareHTML  sets the middelware for html*/
func SetMiddlewareHTML(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		// w.WriteHeader(http.StatusOK)
		next(w, r)
	}
}

func SetMiddlewareJSON(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next(w, r)
	}
}

func SetMiddlewareAuthHTML(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		cookie, err := r.Cookie("jwttoken")
		if err!=nil{
			
			log.Println(err.Error())
			utils.ErrorRenderer(w,errors.New("Unauthorized").Error())
			return
		}else{
			log.Println("Cookie VALUE: ", cookie.Value)
		}
		err = auth.TokenValidByString(cookie.Value)
		if err != nil {
			log.Println("err in auth validation: ",err)
			utils.ErrorRenderer(w,errors.New("Unauthorized").Error())
			return
		}
		next(w, r)

	}
}

func SetMiddlewareAuthentication(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := auth.TokenValid(r)
		if err != nil {
			responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
			return
		}
		next(w, r)
	}
}
