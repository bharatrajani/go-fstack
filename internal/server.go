package api

import (
	"log"

	"github.com/spf13/viper"
	"github.com/bharat-rajani/go-fstack/internal/controllers"
	"github.com/bharat-rajani/go-fstack/configs"
)

var server = controllers.Server{}

func init() {
	// loads values from config.yaml into the system
	configs.InitializeViper()
}

func Run() {

	configs.InitializeViper()
	controllers.InitializeOAuthGoogle()
	log.Println(viper.GetString("mysql.db_driver"),viper.GetString("mysql.db_user"),viper.GetString("mysql.db_password"),viper.GetString("mysql.db_port"),viper.GetString("mysql.host"),viper.GetString("mysql.db_name"))

	server.Initialize(viper.GetString("mysql.db_driver"),viper.GetString("mysql.db_user"),viper.GetString("mysql.db_password"),viper.GetString("mysql.db_port"),viper.GetString("mysql.host"),viper.GetString("mysql.db_name"))


	// seed.Load(server.DB)

	server.Run(":8080")
	log.Println("Server running at 8080")

}