package valueobject

import (
	"errors"

	"github.com/badoux/checkmail"
)


type RegisterObj struct{
	Email			string	`json:"email"`
	Password		string	`json:"password"`
}

func (r *RegisterObj) Validate() error{
	if err := checkmail.ValidateFormat(r.Email); err != nil {
		return errors.New("Invalid Email")
	}

	if err := r.ValidatePassword(); err != nil {
		return errors.New("Invalid Password")
	}
	return nil
}

func (r *RegisterObj) ValidatePassword() error{
	if len([]rune(r.Password)) < 8{
		return errors.New("Password is less than 8 chars.")
	}
	return nil
} 