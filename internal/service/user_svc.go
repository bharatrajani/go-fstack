package service

import (
	"github.com/jinzhu/gorm"
	"github.com/bharat-rajani/go-fstack/internal/models"
)

func CheckUserExistsAndReturn(email string, db *gorm.DB) (*models.User, error){
	// gets a validated email

	user := models.User{}
	user.Email = email
	user.ValidateEmail()
	userGotten, err := user.FindUserByEmail(db, email)
	if err != nil {
		return nil, err
	}
	return userGotten, nil
	
}

// func CreateUserByGid(email string,gid uint32) error{

// 	u := models.User{}
// 	u.Email = email
// 	u.Gid = gid
	
// 	u.Prepare()
// 	err = user.Validate("")
// 	if err != nil {
// 		responses.ERROR(w, http.StatusUnprocessableEntity, err)
// 		return
// 	}
// 	userCreated, err := user.SaveUser(server.DB)

// 	if err != nil {

// 		formattedError := formaterror.FormatError(err.Error())

// 		responses.ERROR(w, http.StatusInternalServerError, formattedError)
// 		return
// 	}
	

// 	return nil
// }



// func CreateUser(email string,password string) error{

// 	u := models.User{}
// 	u.Email = email
// 	u.Password = password
	
// 	userCreated, err := user.SaveUser(server.DB)

// 	if err != nil {

// 		formattedError := formaterror.FormatError(err.Error())

// 		responses.ERROR(w, http.StatusInternalServerError, formattedError)
// 		return
// 	}
// 	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.RequestURI, userCreated.ID))
// 	responses.JSON(w, http.StatusCreated, userCreated)
// 	u.Prepare()

// 	return nil
// }