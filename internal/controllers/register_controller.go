package controllers

import (
	"net/http"
	// "log"
	"html/template"

	// "github.com/bharat-rajani/go-fstack/internal/auth"
	// "github.com/bharat-rajani/go-fstack/internal/utils"
	// "github.com/bharat-rajani/go-fstack/internal/models"
	// "github.com/bharat-rajani/go-fstack/internal/responses"
	templ "github.com/bharat-rajani/go-fstack/web/template"
	// "github.com/jinzhu/gorm"
)


// renders the register page and creates the user if not exists
func (s *Server) Register(w http.ResponseWriter, r *http.Request){
	defer r.Body.Close()

	// cookie, err := r.Cookie("jwttoken")
	// if err!=nil{
	// 	log.Println("cookie not present/error")
	// }
	// userID, email, err := auth.ExtractTokenIDEmailByString(cookie.Value)
	// if err!=nil{
	// 	log.Println("err")
	// 	utils.ErrorRenderer(w,err.Error())
	// 	return
	// }

	// log.Println(userID, email)

	// usr := &models.User{}
	// usr,err = usr.FindUserByID(s.DB,userID)
	// if gorm.IsRecordNotFoundError(err) {
	// 	log.Fatalf("NASTY error, user doesnt exists.\n")
	// }

	// log.Println(usr)
	tmpl := template.Must(templ.RegisterHtml())

	tmpl.Execute(w, nil)
}