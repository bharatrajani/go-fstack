package controllers

import (
	// "encoding/json"
	// "io/ioutil"
	"log"
	"net/http"
	"html/template"

	"github.com/bharat-rajani/go-fstack/internal/auth"
	"github.com/bharat-rajani/go-fstack/internal/utils"
	"github.com/bharat-rajani/go-fstack/internal/models"
	// "github.com/bharat-rajani/go-fstack/internal/responses"
	templ "github.com/bharat-rajani/go-fstack/web/template"
	"github.com/jinzhu/gorm"

)

// this controller gets the authenticated request and renders the profile edit page.
func (s *Server) ProfileEdit_renderer(w http.ResponseWriter, r *http.Request){
	defer r.Body.Close()
	tmpl := template.Must(templ.ProfileEditHtml())

	tmpl.Execute(w, nil)
}


func (s *Server) ProfileShow_renderer(w http.ResponseWriter, r *http.Request){
	defer r.Body.Close()

	cookie, err := r.Cookie("jwttoken")
	if err!=nil{
		log.Println("cookie not present/error")
	}
	userID, email, err := auth.ExtractTokenIDEmailByString(cookie.Value)
	if err!=nil{
		log.Println("err")
		utils.ErrorRenderer(w,err.Error())
		return
	}

	log.Println(userID, email)

	usr := &models.User{}
	usr,err = usr.FindUserByID(s.DB,userID)
	if gorm.IsRecordNotFoundError(err) {
		log.Fatalf("NASTY error, user doesnt exists.\n")
	}

	log.Println(usr)
	tmpl := template.Must(templ.ProfileShowHtml())

	tmpl.Execute(w, usr)
}