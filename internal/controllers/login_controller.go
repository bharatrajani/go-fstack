package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"log"
	"strconv"

	"github.com/bharat-rajani/go-fstack/internal/auth"
	"github.com/bharat-rajani/go-fstack/internal/models"
	"github.com/bharat-rajani/go-fstack/internal/responses"
	"github.com/bharat-rajani/go-fstack/internal/utils/formaterror"
	"golang.org/x/crypto/bcrypt"
)

func (server *Server) Login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("ioutil err: ",err)
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		log.Println("json err: ",err)
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	user.Prepare()
	err = user.Validate("login")
	if err != nil {
		log.Println("user validate err: ",err)
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	token, err, m_user := server.SignIn(user.Email, user.Password)
	if err != nil {
		log.Println("sign in err: ",err)
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusUnprocessableEntity, formattedError)
		return
	}
	m := make(map[string]string)
	m["token"] = token
	m["user_IsProfileCreated"] = strconv.FormatBool(m_user.IsProfileCreated)

	responses.JSON(w, http.StatusOK, m)
}

func (server *Server) SignIn(email, password string) (string, error, models.User) {

	var err error

	user := models.User{}

	err = server.DB.Debug().Model(models.User{}).Where("email = ?", email).Take(&user).Error
	if err != nil {
		return "", err, user
	}
	err = models.VerifyPassword(user.Password, password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err, user
	}

	token, er := auth.CreateToken(user.ID,user.Email)

	return token,er,user
}