package controllers

import (
	"net/http"
	"github.com/bharat-rajani/go-fstack/web/template"
)

func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(template.IndexPage))	
}