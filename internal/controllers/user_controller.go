package controllers

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"errors"
	"log"

	"github.com/bharat-rajani/go-fstack/internal/auth"
	"github.com/bharat-rajani/go-fstack/internal/responses"
	"github.com/bharat-rajani/go-fstack/internal/utils/formaterror"
	"github.com/bharat-rajani/go-fstack/internal/models"
	vo "github.com/bharat-rajani/go-fstack/internal/valueobject"

)

func (server *Server) UpdateUser(w http.ResponseWriter, r *http.Request) {


	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("ioutil err",err)
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	log.Println("req body",string(body))
	user := models.User{}
	err = json.Unmarshal(body, &user)
	if err != nil {
		log.Println("json err",err)
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	log.Println("user obj",user)
	userID,email, err := auth.ExtractTokenIDEmail(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}
	
	user.ID = userID
	user.Email = email
	user.IsProfileCreated = true
	user.Prepare()
	err = user.Validate("update")
	if err != nil {
		log.Println("user validation err",err)
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	updatedUser, err := user.UpdateAUser(server.DB, uint32(userID))
	if err != nil {
		log.Println("updating user err",err)
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	responses.JSON(w, http.StatusOK, updatedUser)
}


func (server *Server) CreateUser(w http.ResponseWriter, r *http.Request) {


	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("ioutil err",err)
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	log.Println("req body",string(body))
	reg := vo.RegisterObj{}
	err = json.Unmarshal(body, &reg)
	if err != nil {
		log.Println("json err",err)
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	log.Println("reg obj",reg)
	if err:=reg.Validate(); err!=nil{
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
	}
	
	user := models.User{}
	user.Email = reg.Email
	user.Password = reg.Password
	user.IsProfileCreated = false
	user.Prepare()
	err = user.Validate("")
	if err != nil {
		log.Println("user validation err",err)
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	
	
	userCreated, err := user.SaveUser(server.DB)
	if err != nil {

		formattedError := formaterror.FormatError(err.Error())

		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	// w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.RequestURI, userCreated.ID))
	responses.JSON(w, http.StatusCreated, userCreated)
}
