package controllers

import (
	"net/http"
	"github.com/bharat-rajani/go-fstack/internal/middlewares"
)

func (s *Server) initializeRoutes() {

	s.Router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("web/static"))))

	s.Router.HandleFunc("/", middlewares.SetMiddlewareHTML(s.Home)).Methods("GET")
	s.Router.HandleFunc("/login-gl",s.HandleGoogleLogin)
	s.Router.HandleFunc("/callback-gl",middlewares.SetMiddlewareHTML(s.CallBackFromGoogle))
	s.Router.HandleFunc("/profile/edit",middlewares.SetMiddlewareAuthHTML(s.ProfileEdit_renderer))
	s.Router.HandleFunc("/profile",middlewares.SetMiddlewareAuthHTML(s.ProfileShow_renderer))
	s.Router.HandleFunc("/logout",middlewares.SetMiddlewareAuthHTML(s.Logout))
	s.Router.HandleFunc("/register",middlewares.SetMiddlewareHTML(s.Register))
	s.Router.HandleFunc("/user/update",middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateUser))).Methods("POST")
	s.Router.HandleFunc("/user/create",middlewares.SetMiddlewareJSON(s.CreateUser)).Methods("POST")
	s.Router.HandleFunc("/login",middlewares.SetMiddlewareJSON(s.Login)).Methods("POST")
}
