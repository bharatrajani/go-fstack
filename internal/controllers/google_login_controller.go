package controllers

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"encoding/json"
	"fmt"
	"log"
	"time"
	

	
	"github.com/spf13/viper"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"github.com/bharat-rajani/go-fstack/internal/service"
	"github.com/bharat-rajani/go-fstack/internal/utils"
	"github.com/bharat-rajani/go-fstack/internal/models"
	"github.com/bharat-rajani/go-fstack/internal/auth"
	"github.com/bharat-rajani/go-fstack/internal/responses"
	"github.com/jinzhu/gorm"
)



var (
	oauthConfGl = &oauth2.Config{
		ClientID:     "",
		ClientSecret: "",
		RedirectURL:  "http://localhost:8080/callback-gl",
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint:     google.Endpoint,
	}
	oauthStateStringGl = ""
)

/*
HandleGoogleLogin Function
// function to load the configs. TODO: refactor
*/
func InitializeOAuthGoogle() {
	oauthConfGl.ClientID = viper.GetString("google.clientID")
	oauthConfGl.ClientSecret = viper.GetString("google.clientSecret")
	oauthStateStringGl = viper.GetString("oauthStateString")
}

/*
HandleGoogleLogin Function
*/
func (server *Server) HandleGoogleLogin(w http.ResponseWriter, r *http.Request) {
	fmt.Println("#############################")
	fmt.Println("=============================")
	fmt.Println(oauthConfGl.ClientID)
	// logger.Log.Info(oauthStateStringGl)
	fmt.Println("#######************************")
	URL, err := url.Parse(oauthConfGl.Endpoint.AuthURL)
	if err != nil {
		log.Println("Parse: " + err.Error())
	}
	log.Println(URL.String())
	parameters := url.Values{}
	parameters.Add("client_id", oauthConfGl.ClientID)
	parameters.Add("scope", strings.Join(oauthConfGl.Scopes, " "))
	parameters.Add("redirect_uri", oauthConfGl.RedirectURL)
	parameters.Add("response_type", "code")
	parameters.Add("state", oauthStateStringGl)
	URL.RawQuery = parameters.Encode()
	url := URL.String()
	log.Println(url)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}


/*
*  Handler to receive callback from google at callback-gl route
*/
func (server *Server) CallBackFromGoogle(w http.ResponseWriter, r *http.Request) {
	log.Println("Callback-gl..")

	state := r.FormValue("state")
	log.Println(state)
	if state != oauthStateStringGl {
		log.Println("invalid oauth state, expected " + oauthStateStringGl + ", got " + state + "\n")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	code := r.FormValue("code")
	log.Println(code)

	if code == "" {
		log.Println("Code not found..")
		w.Write([]byte("Code Not Found to provide AccessToken..\n"))
		reason := r.FormValue("error_reason")
		if reason == "user_denied" {
			w.Write([]byte("User has denied Permission.."))
		}
		// User has denied access..
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	} else {
		token, err := oauthConfGl.Exchange(oauth2.NoContext, code)
		if err != nil {
			utils.ErrorRenderer(w,err.Error())
			log.Println("oauthConfGl.Exchange() failed with " + err.Error() + "\n")
			return
		}
		log.Println("TOKEN>> AccessToken>> " + token.AccessToken)
		log.Println("TOKEN>> Expiration Time>> " + token.Expiry.String())
		log.Println("TOKEN>> RefreshToken>> " + token.RefreshToken)

		resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + url.QueryEscape(token.AccessToken))
		if err != nil {
			log.Println("Get: " + err.Error() + "\n")
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println("ReadAll: " + err.Error() + "\n")
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}

		log.Println("parseResponseBody: " + string(body) + "\n")

		

		googleResp := responses.GoogleResp{}
		err = json.Unmarshal(body, &googleResp)
		if err != nil {
			utils.ErrorRenderer(w,err.Error())
			return
		}

		usr, err := service.CheckUserExistsAndReturn(googleResp.Email,server.DB)
		if gorm.IsRecordNotFoundError(err) {
			user := models.User{}
			log.Println("Creating user",usr,googleResp)
			user.Gid = googleResp.ID
			user.Email = googleResp.Email
			log.Println("Validating user")
			err := user.Validate("google")
			if err != nil {
				log.Println(err)
				utils.ErrorRenderer(w,err.Error())
				return
			}
			usr,err = user.SaveUser(server.DB)
			if err != nil {
				log.Println(err)
				utils.ErrorRenderer(w,err.Error())
				return
			}

		}else if err!=nil{
			log.Println(err)
			utils.ErrorRenderer(w,err.Error())
			return
		}
		

		fmt.Println("USER --> ",usr)
		jwtToken, tokenErr := auth.CreateToken(usr.ID,usr.Email)
		fmt.Println("=== jwt === ", jwtToken)
		if tokenErr!=nil{
			fmt.Println("Unable to create token")
			log.Println(err)
			utils.ErrorRenderer(w,err.Error())
			return
		}
		
		expiration := time.Now().Add(365 * 24 * time.Hour)
		fmt.Println(expiration)
		cookie    :=    http.Cookie{Name: "jwttoken",Value:jwtToken,Expires:expiration}
		http.SetCookie(w, &cookie)

		if (usr.IsProfileCreated){
			// create token and redirect to profile view page
			fmt.Println("profile is created already")
			http.Redirect(w, r, "/profile", http.StatusTemporaryRedirect)
			return
		}else{
			
			
			http.Redirect(w, r, "/profile/edit", http.StatusTemporaryRedirect)
			return
		}
		return
	}
}