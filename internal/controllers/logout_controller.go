package controllers

import (
	"net/http"
	"log"
)

func (s *Server) Logout(w http.ResponseWriter, r *http.Request) {

	log.Println("Logging out..")
	
	c := http.Cookie{
		Name:   "jwttoken",
		MaxAge: -1,
	}
	http.SetCookie(w, &c)
	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}