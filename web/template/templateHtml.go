package template


import (
	"html/template"
)

func ErrorHtml() (*template.Template,error){
	return template.ParseFiles("web/static/error.html")
}

func ProfileEditHtml() (*template.Template,error){
	return template.ParseFiles("web/static/profile_edit.html")
}

func ProfileShowHtml() (*template.Template,error){
	return template.ParseFiles("web/static/profile.html")
}

func RegisterHtml() (*template.Template,error){
	return template.ParseFiles("web/static/register.html")
}