package template

import(
	"fmt"
	"io/ioutil"
)


/*
IndexPage renders the html content for the index page.
*/
var IndexPage = `Error`


func init(){
	html, err := ioutil.ReadFile("web/static/index.html") // just pass the file name
    if err != nil {
        fmt.Print(err)
	}
	
	IndexPage = string(html)
}