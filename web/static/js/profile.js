(function ($) {
    "use strict";


    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function( event){
        event.preventDefault();
        console.log("input: ",input);
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        if(check){
            var profileObj = {}
            for(var i=0; i<input.length; i++) {
                console.log($(input[i]).attr('name'),$(input[i]).val().trim())
                profileObj[$(input[i]).attr('name')] = $(input[i]).val().trim();
            }
            // profileObj["telephone"] = Number(profileObj["telephone"]);
            console.log(profileObj);
            var $form = $(this),
            url = $form.attr('action');
            const token = Cookies.get("jwttoken");
            console.log(token);
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: JSON.stringify(profileObj),
                headers: {
                    'Authorization': 'Bearer '+token,   //If your header name has spaces or any other char not appropriate
                    'Content-Type': 'application/json',  //for object property name, use quoted notation shown in second
                },
                dataType: 'json',
                success: function (data, textStatus, xhr) {
                    console.info(data, textStatus, xhr);
                    if(xhr.status === 200){
                        window.location.href = "/profile"
                    }
                },
                error: function (data){
                    console.info("failed",data)
                    alert("User profile creation/updation failed, please try again.");        
                }
            });
        }


        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
        hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'full_name' || $(input).attr('name') == 'full_name') {
            if($(input).val().trim() == '' || $(input).val().trim().length >= 255 ) {
                return false;
            }
        }
        else if($(input).attr('type') == 'address' || $(input).attr('name') == 'address') {
            if($(input).val().trim() == '' || $(input).val().trim().length >= 255 ) {
                return false;
            }
        }
        else if($(input).attr('type') == 'telephone' || $(input).attr('name') == 'telephone') {
            console.log(!$.isNumeric($(input).val().trim()))
            if($(input).val().trim().length != 10 && !$.isNumeric($(input).val().trim())) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == '') {
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    

})(jQuery);

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

