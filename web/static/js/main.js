
(function ($) {
    "use strict";


    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(event){
        event.preventDefault();
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        if(check){
            var userObj = {}
            for(var i=0; i<input.length; i++) {
                console.log($(input[i]).attr('name'),$(input[i]).val().trim())
                userObj[$(input[i]).attr('name')] = $(input[i]).val().trim();
            }
            // userObj["telephone"] = Number(userObj["telephone"]);
            // delete userObj["c_password"]
            console.log(userObj);
            var $form = $(this),
            url = $form.attr('action');
            // const token = Cookies.get("jwttoken");
            // console.log(token);
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: JSON.stringify(userObj),
                headers: {
                    'Content-Type': 'application/json',
                },
                dataType: 'json',
                success: function (data, textStatus, xhr) {
                    console.info(data, textStatus, xhr);
                    
                    if(xhr.status === 200){
                        console.log(data)
                        Cookies.set("jwttoken",data["token"])
                        alert("Succesfully logged in.")
                        if(data["user_IsProfileCreated"] === "true"){
                            window.location.href = "/profile"
                        }else{
                            window.location.href = "/profile/edit"
                        }
                        
                    }
                    
                },
                error: function (data){
                    console.info("failed",data)
                    alert("User login failed, check email and please try again.");        
                }
            });
        }


        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else if($(input).attr('type') == 'password' || $(input).attr('name') == 'password') {
            if($(input).val().trim() == '' || $(input).val().trim().length < 8 ) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    

})(jQuery);