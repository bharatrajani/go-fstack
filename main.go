package main

import (

	"log"
	// "net/http"
	api "github.com/bharat-rajani/go-fstack/internal"
	
)

func main(){

	log.Println("Starting the application...")
	
	api.Run()

}