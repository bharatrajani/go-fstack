# Go-FSTACK


Go Fullstack:
This application is an authorization/authentication app which manages custom profile post login.

The database used with this app in heroku is db4free.net.
Since this is a free test DB hence it is expected that it can be down while you visit https://go-fstack.herokuapp.com/.


To run the applicaton you must have a mysql DB.

```
docker run --name br-mysql -e MYSQL_ROOT_PASSWORD=rajani21 -d mysql:latest
```

> In config.yaml do the changes accordingly.


#### To run this application:

```
go run ./main.go
```

#### To run unit tests:

```
go test -v ./...
```


### Below are the screenshots of the application .

#### Login with google.

![Login](docs/images/1.png)
![Login](docs/images/2.png)
![Login](docs/images/3.png)
![Login](docs/images/4.png)
![Login](docs/images/5.png)
![Login](docs/images/6.png)

#### Logout

![Login](docs/images/7.png)

#### Registering as normal user and repeating whole flow.
![Login](docs/images/8.png)
![Login](docs/images/9.png)
![Login](docs/images/10.png)
![Login](docs/images/11.png)

![Login](docs/images/12.png)

#### Clicking on ok in alert redirects the user to profile edit page if he/she has never saved his/her profile.
![Login](docs/images/13.png)
![Login](docs/images/14.png)
![Login](docs/images/15.png)
![Login](docs/images/16.png)
![Login](docs/images/17.png)
![Login](docs/images/18.png)


#### Entries in mysql
![Login](docs/images/19.png)

#### After update
![Login](docs/images/20.png)


----


## Note:

- Frontend is rendered as template and all the state changes are happening with REST.

    For example

    On Profile edit form submit it will call jquery ajax on url "/user/update" with authorization bearer JWT token in header.

- Once a user is logging in by google or manually, we are storing a cookie at browser which contains JWT.

    With every subsequent request this token is sent.
    If not then an unauthorized error page is rendered.

- Once callback is received from google, then a user is created in DB and he/she is redirected to profile view page.
    If they are have never created profile then it will redirect to profile edit page after login.

- REST calls are happening at client side , Jquery ajax calls happen on form submit and the token is fetched from cookie and sent with the header to REST API endpoint. 
    This decouples the app.

- JWT claims contain userID and email.








